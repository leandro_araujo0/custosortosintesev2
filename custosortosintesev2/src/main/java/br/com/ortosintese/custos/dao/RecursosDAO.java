package br.com.ortosintese.custos.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.ortosintese.custos.models.Recurso;

@Repository
@Transactional
public class RecursosDAO {

	@PersistenceContext
	private EntityManager manager;

	public void salva(Recurso recurso) {
		if (manager.find(Recurso.class, recurso.getId()) == null) {
			manager.persist(recurso);
		}
	}

	public List<Recurso> findAll() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Recurso> query = criteriaBuilder.createQuery(Recurso.class);
		Root<Recurso> root = query.from(Recurso.class);

		query.select(root);
		query.orderBy(criteriaBuilder.asc(root.get("descricao")));

		return manager.createQuery(query).getResultList();
	}
}
