package br.com.ortosintese.custos.conf;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
public class JPAConfiguration {

	@Bean
	public LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean() {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();

		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		factoryBean.setJpaVendorAdapter(vendorAdapter);

		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
//		dataSource.setUrl("jdbc:hsqldb:hsql://localhost/bcustosorto");
//		dataSource.setUsername("SA");
//		dataSource.setPassword("");
//		dataSource.setDriverClassName("org.hsqldb.jdbcDriver");
//		String dialect = "org.hibernate.dialect.HSQLDialect";
		
		dataSource.setUrl("jdbc:mysql://localhost/bcustosorto");
		dataSource.setUsername("root");
		dataSource.setPassword("rufle121");
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		String dialect = "org.hibernate.dialect.MySQL5Dialect";
		
//		dataSource.setUrl("jdbc:mysql://localhost/custosortoequip");
//		dataSource.setUsername("root");
//		dataSource.setPassword("rufle121");
//		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
//		String dialect = "org.hibernate.dialect.MySQL5Dialect";
		
		factoryBean.setDataSource(dataSource);

		Properties prop = new Properties();
		prop.setProperty("hibernate.dialect", dialect);
		prop.setProperty("hibernate.show_sql", "true");
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		factoryBean.setJpaProperties(prop);

		factoryBean.setPackagesToScan("br.com.ortosintese.custos.models");

		return factoryBean;
	}

	@Bean
	public JpaTransactionManager jpaTransactionManager(EntityManagerFactory emf) {
		return new JpaTransactionManager(emf);
	}
}
