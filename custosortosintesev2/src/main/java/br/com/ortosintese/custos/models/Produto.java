package br.com.ortosintese.custos.models;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.springframework.format.annotation.DateTimeFormat;

import br.com.ortosintese.custos.dao.ProdutoDAO;

@Entity
public class Produto {

	@Id
	private String codigo;
	private String descricao;
	private float markup = 2.0f;
	@OneToOne
	private MateriaPrima materiaPrima;
	private double qtdMateriaPrima;
	@OneToMany(fetch = FetchType.EAGER)
	private Set<Processo> processos;
	@OneToMany(fetch = FetchType.EAGER)
	private Set<Componente> componentes;
	@DateTimeFormat
	private Calendar revisao;

	public BigDecimal getCusto() {
		BigDecimal custoTotal = new BigDecimal(0);
		custoTotal = custoTotal.add(getCustoMateriaPrima());
		custoTotal = custoTotal.add(getCustoProcessos());
		custoTotal = custoTotal.add(getCustoComponentes());
		return custoTotal.setScale(2, RoundingMode.HALF_EVEN);
	}

	public BigDecimal getCustoComponentes() {
		BigDecimal custoTotalComponentes = new BigDecimal(0);

		for (Componente componente : componentes) {
			custoTotalComponentes = custoTotalComponentes.add(componente.getCustoTotal());
		}

		return custoTotalComponentes;
	}

	public BigDecimal getCustoMateriaPrima() {
		try {
			return materiaPrima.getCustoPorUM().multiply(new BigDecimal(qtdMateriaPrima)).setScale(2,
					RoundingMode.HALF_EVEN);
		} catch (Exception e) {
			return new BigDecimal(0).setScale(2, RoundingMode.HALF_EVEN);
		}
	}

	public BigDecimal getCustoProcessos() {
		BigDecimal custoTotalProcessos = new BigDecimal(0);

		for (Processo processo : processos) {
			custoTotalProcessos = custoTotalProcessos.add(processo.getCusto());
		}

		return custoTotalProcessos;
	}

	public String getCodigo() {
		return codigo;
	}

	public Produto setCodigo(String codigo) {
		this.codigo = codigo;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public MateriaPrima getMateriaPrima() {
		return materiaPrima;
	}

	public Produto setMateriaPrima(MateriaPrima materiaPrima) {
		this.materiaPrima = materiaPrima;
		return this;
	}

	public double getQtdMateriaPrima() {
		return qtdMateriaPrima;
	}

	public void setQtdMateriaPrima(double qtdMateriaPrima) {
		this.qtdMateriaPrima = qtdMateriaPrima;
	}

	public Set<Processo> getProcessos() {
		return processos;
	}

	public void setProcessos(Set<Processo> processos) {
		this.processos = processos;
	}

	public Set<Componente> getComponentes() {
		return componentes;
	}

	public void setComponentes(Set<Componente> componentes) {
		this.componentes = componentes;
	}

	public Calendar getRevisao() {
		return revisao;
	}

	public void setRevisao(Calendar revisao) {
		this.revisao = revisao;
	}

	@Override
	public String toString() {
		return "Produto [codigo=" + codigo + ", descricao=" + descricao + ", materiaPrima=" + materiaPrima
				+ ", qtdMateriaPrima=" + qtdMateriaPrima + ", processos=" + processos + ", componentes=" + componentes
				+ ", revisao=" + revisao + "]";
	}

	public void addProcesso(Processo processo) {
		if (!processos.contains(processo)) {
			processos.add(processo);
		} else {
			processos.remove(processo);
			processos.add(processo);
		}
	}

	public void removeProcesso(String fornecedor, String descricao) {
		Iterator<Processo> iterator = processos.iterator();
		while (iterator.hasNext()) {
			Processo tempProcesso = iterator.next();
			if (tempProcesso.getFornecedor().equals(fornecedor)) {
				if (tempProcesso.getDescricao().equals(descricao)) {
					iterator.remove();
				}
			}
		}
	}

	public void alteraProcesso(Processo processoAntigo, Processo processoNovo) {
		Iterator<Processo> iterator = processos.iterator();
		while (iterator.hasNext()) {
			Processo processo = iterator.next();
			if (processo.getFornecedor().equals(processoAntigo.getFornecedor())) {
				if (processo.getDescricao().equals(processoAntigo.getDescricao())) {
					iterator.remove();
				}
			}
		}
		processos.add(processoNovo);
	}

	public void addComponente(Componente componente) {
		componentes.add(componente);
	}

	public void removeComponente(Componente componente) {
		componentes.remove(componente);
	}

	public void removeMateriaPrima() {
		materiaPrima = null;
		qtdMateriaPrima = 0;
	}

	public Processo getProcesso(String fornecedor, String descricao) {
		for (Processo processo : processos) {
			if (processo.getFornecedor().equals(fornecedor)) {
				if (processo.getDescricao().equals(descricao)) {
					return processo;
				}
			}
		}
		return null;
	}

	public void atualizaRevisao() {
		this.revisao = Calendar.getInstance();
	}

	public BigDecimal getPreco(ProdutoDAO dao, String mercado) {
		BigDecimal preco = dao.getPreco(codigo, mercado);
		return preco;
	}

	public boolean getCustoZero() {
		return (getCusto().equals(new BigDecimal(0).setScale(2))) ? true : false;
	}

	public float getMarkup() {
		return markup;
	}

	public void setMarkup(float markup) {
		this.markup = markup;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		return true;
	}
}
