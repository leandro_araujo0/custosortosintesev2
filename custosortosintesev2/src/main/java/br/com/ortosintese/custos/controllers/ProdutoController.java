package br.com.ortosintese.custos.controllers;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.ortosintese.custos.conf.AppWebConfiguration;
import br.com.ortosintese.custos.dao.MateriaPrimaDAO;
import br.com.ortosintese.custos.dao.ProdutoDAO;
import br.com.ortosintese.custos.dao.RecursosDAO;
import br.com.ortosintese.custos.models.Componente;
import br.com.ortosintese.custos.models.Processo;
import br.com.ortosintese.custos.models.Produto;

@Controller
@RequestMapping("/produtos")
public class ProdutoController {

	@Autowired
	private ProdutoDAO dao;
	@Autowired
	private MateriaPrimaDAO materiaPrimaDAO;
	@Autowired
	private RecursosDAO recursosDAO;
	@Autowired
	private ProcessoDAO processoDAO;

	@RequestMapping(value = "lista", method = RequestMethod.GET)
	public String lista(String codigo, String descricao, Model model) {
		List<Produto> produtos = dao.findByCodigoAndDescricao(codigo, descricao);
		model.addAttribute("produtos", produtos);
		model.addAttribute("dateFormat", new SimpleDateFormat("dd/MM/yyyy"));
		model.addAttribute("dao", dao);
		return "produtos/lista";
	}

	@RequestMapping("/")
	public String home(Model model) {
		model.addAttribute("produtos", dao.findLastFive());
		model.addAttribute("dateFormat", new SimpleDateFormat("dd/MM/yyyy"));
		model.addAttribute("dao", dao);
		return "produtos/home";
	}

	@RequestMapping(value = "form")
	public String form(Produto produto, Model model) {
		model.addAttribute("data", new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime()));
		return "produtos/form";
	}

	@RequestMapping(value = "cadastro", method = RequestMethod.POST)
	public String cadastro(Produto produto) {
		dao.salva(produto);
		return redirectDetalhe(produto.getCodigo());
	}

	@RequestMapping(value = "alteraDescricao", method = RequestMethod.POST)
	public String alteraDescricao(String codigo, String descricao) {
		dao.alteraDescricao(codigo, descricao);
		return redirectDetalhe(codigo);
	}

	@RequestMapping(value = "detalhe")
	public String detalhe(String codigo, Model model) {
		model.addAttribute("produto", dao.find(codigo));
		model.addAttribute("dateFormat", new SimpleDateFormat("dd/MM/yyyy"));
		model.addAttribute("materiasPrimas", materiaPrimaDAO.findall());
		model.addAttribute("dao", dao);
		return "produtos/detalhe";
	}

	private String redirectDetalhe(String codigo) {
		return "redirect:/produtos/detalhe?codigo=" + codigo;
	}

	@RequestMapping(value = "addProcesso", method = RequestMethod.GET)
	public String processoForm(String codigo, Model model, Processo processo) {
		Produto produto = new Produto();
		produto.setCodigo(codigo);
		model.addAttribute("produto", produto);
		model.addAttribute("processo", processo);
		model.addAttribute("recursos", recursosDAO.findAll());
		model.addAttribute("actionName", "PC#addProcesso");
		model.addAttribute("btnName", "Cadastrar");
		return "produtos/processosForm";
	}

	@RequestMapping(value = "addProcesso", method = RequestMethod.POST)
	public String addProcesso(Processo processo, String codigo) {
		Produto produto = new Produto().setCodigo(codigo.split(",")[1]);
		processo.setProduto(produto);
		processoDAO.salva(processo);
		dao.addProcesso(processo);
		return redirectDetalhe(processo.getProduto().getCodigo());
	}

	@RequestMapping(value = "alteraProcesso", method = RequestMethod.GET)
	public String alteraProcessoForm(Long id, Model model) {
		Processo processo = processoDAO.find(id);
		model.addAttribute("produto", processo.getProduto());
		model.addAttribute("processo", processo);
		model.addAttribute("recursos", recursosDAO.findAll());
		model.addAttribute("actionName", "PC#alteraProcesso");
		model.addAttribute("btnName", "Alterar");
		return "produtos/processosForm";
	}

	@RequestMapping(value = "alteraProcesso", method = RequestMethod.POST)
	public String alteraProcesso(Processo processo) {
		processoDAO.salva(processo);
		return redirectDetalhe(processo.getProduto().getCodigo());
	}

	@RequestMapping(value = "removeMateriaPrima", method = RequestMethod.GET)
	public String removeMateriaPrima(String codigo) {
		dao.removeMateriaPrima(codigo);
		return redirectDetalhe(codigo);
	}

	@RequestMapping(value = "alteraMateriaPrima", method = RequestMethod.POST)
	public String alteraMateriaPrima() {
		// TODO refazer o método de alterar matéria prima
		return null;
	}

	@RequestMapping("/removeProcesso")
	public String removeProcesso(long id) {
		processoDAO.remove(id);
		String codigo = processoDAO.find(id).getProduto().getCodigo();
		return redirectDetalhe(codigo);
	}

	@RequestMapping(value = "/addComponente", method = RequestMethod.GET)
	public String cadastraComponenteForm(Model model, String codigo, Componente componente) {
		Produto produto = dao.find(codigo);
		model.addAttribute("produto", produto);
		model.addAttribute(componente);
		return "produtos/componenteForm";
	}

	@RequestMapping(value = "/addComponente", method = RequestMethod.POST)
	public String cadastraComponente() {
		// TODO refazer o método de cadastrar componente
		return null;
	}

	@RequestMapping(value = "/alteraComponente", method = RequestMethod.GET)
	public ModelAndView alteraComponenteForm() {
		// TODO refazer
		return null;
	}

	@RequestMapping(value = "/alteraComponente", method = RequestMethod.POST)
	public String alteraComponente(String codigo) {
		// TODO refazer o métodod e alteração de componente
		return null;
	}

	@RequestMapping(value = "removeComponente", method = RequestMethod.GET)
	public String removeComponente(String codigoProduto, String codigoProdutoFilho) {
		// TODO refazer o método de remover componente
		return null;
	}

	public ModelAndView componenteForm() {
		// TODO refazer como o form de componente é mostrado
		return null;
	}

	@RequestMapping(value = "/excluirProduto", method = RequestMethod.GET)
	public String excluirProduto(String codigo) {
		dao.excluiProduto(codigo);
		return "redirect:/produtos/lista";
	}

	@RequestMapping(value = "/atualizarPrecos", method = RequestMethod.GET)
	public String atualizarPrecosForm() {
		return "produtos/atualizarPrecosForm";
	}

	@RequestMapping(value = "/atualizarPrecos", method = RequestMethod.POST)
	public String atualizarPrecos(String mercado, String precos) {
		dao.atualizaPrecos(mercado, precos);
		return "redirect:/produtos/";
	}

	@RequestMapping(value = "/impressao")
	public String impressao(String codigo, Model model) {
		Produto produto = dao.find(codigo);
		model.addAttribute("produto", produto);
		return "produtos/impressao";
	}

	@RequestMapping(value = "/criticos")
	public String produtosCriticos(Model model) {
		List<Produto> produtos = dao.findAll();
		for (int i = 0; i < produtos.size(); i++) {
			Produto produto = produtos.get(i);
			try {
				BigDecimal precoMI = dao.getPreco(produto.getCodigo(), "MI");
				BigDecimal precoME = dao.getPreco(produto.getCodigo(), "ME");
				if (precoMI.divide(produto.getCusto(), 2).compareTo(new BigDecimal(produto.getMarkup())) > 0) {
					if (precoME.multiply(AppWebConfiguration.cotacao).divide(produto.getCusto())
							.compareTo(new BigDecimal(produto.getMarkup())) > 0) {
						produtos.remove(produto);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				produtos.remove(produto);
				continue;
			}
		}
		model.addAttribute("produtos", produtos);
		model.addAttribute("dao", dao);
		return "produtos/criticos";
	}
}
