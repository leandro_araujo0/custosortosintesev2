//package br.com.ortosintese.custos.conf;
//
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
//
//@EnableWebMvcSecurity
//public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
//	
//	@Override
//	protected void configure(HttpSecurity http) throws Exception {
//		// formatter:off
//		http.authorizeRequests()
//		.antMatchers("*").hasRole("USER")
//		.and().formLogin();
//	}
//}
