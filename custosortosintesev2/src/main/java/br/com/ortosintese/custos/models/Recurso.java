package br.com.ortosintese.custos.models;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Recurso {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String descricao;
	private BigDecimal custoPorHora;

	public Recurso() {
	}

	public Recurso(String descricao, BigDecimal custoPorHora) {
		this.descricao = descricao;
		this.custoPorHora = custoPorHora;
	}

	public Recurso(String recurso) {
		descricao = recurso;
	}

	public Recurso(String descricao, int i) {
		this.descricao = descricao;
		this.custoPorHora = new BigDecimal(i);
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public BigDecimal getCustoPorHora() {
		return custoPorHora;
	}

	public void setCustoPorHora(BigDecimal custoPorHora) {
		this.custoPorHora = custoPorHora;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	public static Recurso findByDescricao(List<Recurso> recursos, String descricao) {
		for (Recurso recurso : recursos) {
			if (recurso.descricao.equals(descricao)) {
				return recurso;
			}
		}
		return null;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
