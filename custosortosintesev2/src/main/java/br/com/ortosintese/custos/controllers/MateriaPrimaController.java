package br.com.ortosintese.custos.controllers;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.ortosintese.custos.dao.MateriaPrimaDAO;
import br.com.ortosintese.custos.models.MateriaPrima;
import br.com.ortosintese.custos.models.UnidadeMedida;

@Controller
@RequestMapping("/materiaPrima")
public class MateriaPrimaController {
	
	@Autowired
	private MateriaPrimaDAO dao;

	@RequestMapping(value = "/lista", method = RequestMethod.GET)
	public String lista(String descricao, UnidadeMedida unidadeMedida, BigDecimal valorMenor, BigDecimal valorMaior, Model model) {
		model.addAttribute("unidadesMedidas", UnidadeMedida.values());
		if (descricao != null) {
		descricao = descricao.replace(" ", "%");
		}
		model.addAttribute("materiasPrimas", dao.findByDescricaoAndUnidadeMedidaAndValor(descricao, unidadeMedida, valorMenor, valorMaior));
		return "materiaPrima/lista";
	}
	
	@RequestMapping("/{id}/detalhes")
	public String detalhes(@PathVariable int id, Model model) {
		MateriaPrima materiaPrima = dao.find(id);
		model.addAttribute("materiaPrima", materiaPrima);
		return "materiaPrima/detalhes";
	}
	
	@RequestMapping(value = "/alteraMateriaPrima", method = RequestMethod.POST)
	public String alteraMateriaPrima(MateriaPrima materiaPrima) {
		dao.altera(materiaPrima);
		return "redirect:/materiaPrima/lista/";
	}
	
	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public String form(MateriaPrima materiaPrima, Model model) {
		model.addAttribute("materiaPrima", materiaPrima);
		model.addAttribute("unidadesMedidas", UnidadeMedida.values());
		return "materiaPrima/form";
	}
	
	@RequestMapping(value = "/cadastrar", method=RequestMethod.POST)
	public String cadastrar(MateriaPrima materiaPrima) {
		dao.persist(materiaPrima);
		return "redirect:/materiaPrima/lista";
	}
	
	@RequestMapping(value = "/removeMateriaPrima")
	public String removeMateriaPrima (int id) {
		dao.remove(id);
		return "redirect:/materiaPrima/lista";
	}
}