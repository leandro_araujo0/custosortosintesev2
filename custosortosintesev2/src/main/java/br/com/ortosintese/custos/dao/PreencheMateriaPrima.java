package br.com.ortosintese.custos.dao;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.ortosintese.custos.models.MateriaPrima;
import br.com.ortosintese.custos.models.UnidadeMedida;

@Repository
@Transactional
public class PreencheMateriaPrima {

	@PersistenceContext
	EntityManager manager;
	private static List<MateriaPrima> materiasPrimas;

	public static void main(String[] args) {
	}

	public static List<MateriaPrima> getMateriasPrimas() {
		String materiaPrimaSql = "C:\\Users\\leandro\\git\\custosortosintesev2\\custosortosintesev2\\src\\main\\webapp\\resources\\MateriaPrima.sql";
//		String materiaPrimaSql = "/var/lib/tomcat7/webapps/co/resources/MateriaPrima.sql";
		
		try (Scanner scanner = new Scanner(new File(materiaPrimaSql))) {
			materiasPrimas = new LinkedList<>();
			while (scanner.hasNext()) {
				String line = scanner.nextLine();
				MateriaPrima materiaPrima = new MateriaPrima();
				materiaPrima.setDescricao(line.split(";")[0].replaceAll("Ã˜", "Ø"));
				materiaPrima.setUnidadeMedida(UnidadeMedida.valueOf(line.split(";")[1]));
				materiaPrima.setCustoPorUM(new BigDecimal(line.split(";")[2]));
				materiasPrimas.add(materiaPrima);
			}
			return materiasPrimas;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return materiasPrimas;
	}
}