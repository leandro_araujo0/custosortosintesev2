package br.com.ortosintese.custos.models;

import java.text.DecimalFormat;

public class Ferramentas {

	public static String tempoDoubleToHora(double tempo) {
		int horas = (int) tempo;
		int minutos = (int) ((tempo % 1) * 60);
		int segundos = (int) (((tempo % 1) * 60) % 1) * 60;
		DecimalFormat fmt = new DecimalFormat("00");
		return new String(fmt.format(horas) + ":" + fmt.format(minutos) + ":" + fmt.format(segundos));
	}

}
