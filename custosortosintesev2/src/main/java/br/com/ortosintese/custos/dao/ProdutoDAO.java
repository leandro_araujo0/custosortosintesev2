package br.com.ortosintese.custos.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.ortosintese.custos.models.Componente;
import br.com.ortosintese.custos.models.Preco;
import br.com.ortosintese.custos.models.Processo;
import br.com.ortosintese.custos.models.Produto;

@Repository
@Transactional
public class ProdutoDAO {

	@PersistenceContext
	EntityManager manager;

	public void salva(Produto produto) {
		if (manager.find(Produto.class, produto.getCodigo()) == null) {
			produto.atualizaRevisao();
			manager.persist(produto);
		}
	}

	public void altera(Produto produto) {
		produto.atualizaRevisao();
		manager.merge(produto);
	}

	public Produto find(String codigo) {
		return manager.find(Produto.class, codigo);
	}

	@SuppressWarnings("unchecked")
	public List<Produto> pegaApenasProdutos() {
		Query query = manager.createQuery("select p from Produto p");
		List<Produto> produtos = query.getResultList();
		return produtos;
	}

	public void excluiProduto(String codigo) {
		Produto produto = find(codigo);
		manager.remove(produto);
	}

	public void removeProcesso(String codigo, String fornecedor, String descricao) {
		Produto produto = manager.find(Produto.class, codigo);
		produto.removeProcesso(fornecedor, descricao);
		produto.atualizaRevisao();
	}

	public void removeComponente(String codigoProduto, Componente componente) {
		Produto produto = manager.find(Produto.class, codigoProduto);
		produto.removeComponente(componente);
		produto.atualizaRevisao();
	}

	public void adicionaProcesso(String codigo, Processo processo) {
		Produto produto = manager.find(Produto.class, codigo);
		produto.addProcesso(processo);
		produto.atualizaRevisao();
	}

	public List<Produto> findByCodigoAndDescricao(String codigo, String descricao) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> query = criteriaBuilder.createQuery(Produto.class);
		Root<Produto> root = query.from(Produto.class);

		ArrayList<Predicate> predicates = new ArrayList<>();

		if (codigo != null && codigo != "") {
			Path<String> codigoPath = root.<String> get("codigo");
			Predicate predicateCodigoLike = criteriaBuilder.like(codigoPath, "%" + codigo + "%");
			predicates.add(predicateCodigoLike);
		}

		if (descricao != null && descricao != "") {
			Path<String> descricaoPath = root.<String> get("descricao");
			Predicate predicateDescricaoLike = criteriaBuilder.like(descricaoPath, "%" + descricao + "%");
			predicates.add(predicateDescricaoLike);
		}

		query.where(predicates.toArray(new Predicate[0]));

		query.select(root);
		query.orderBy(criteriaBuilder.asc(root.<String> get("codigo")));

		return manager.createQuery(query).getResultList();
	}

	public List<Produto> findAll() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> query = criteriaBuilder.createQuery(Produto.class);
		Root<Produto> root = query.from(Produto.class);

		query.select(root);
		query.orderBy(criteriaBuilder.asc(root.get("codigo")));

		return manager.createQuery(query).getResultList();
	}

	public void alteraProcesso(String codigo, Processo processoAntigo, Processo processoNovo) {
		Produto produto = manager.find(Produto.class, codigo);
		produto.alteraProcesso(processoAntigo, processoNovo);
		produto.atualizaRevisao();
	}

	public List<Produto> findLastFive() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Produto> query = criteriaBuilder.createQuery(Produto.class);
		Root<Produto> root = query.from(Produto.class);
		query.select(root);
		Path<Object> pathRevisao = root.get("revisao");
		Order order = criteriaBuilder.desc(pathRevisao);
		query.orderBy(order);
		return manager.createQuery(query).setMaxResults(10).getResultList();
	}

	public BigDecimal getPreco(String codigo, String mercado) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<Preco> query = criteriaBuilder.createQuery(Preco.class);
		Root<Preco> root = query.from(Preco.class);

		ArrayList<Predicate> predicates = new ArrayList<>();

		Path<String> codigoPath = root.<String> get("codigo");
		Predicate codigoEqual = criteriaBuilder.equal(codigoPath, codigo);
		predicates.add(codigoEqual);

		Path<String> mercadoPath = root.<String> get("mercado");
		Predicate mercadoEqual = criteriaBuilder.equal(mercadoPath, mercado);
		predicates.add(mercadoEqual);

		query.where(predicates.toArray(new Predicate[0]));

		try {
			Preco preco = manager.createQuery(query).getSingleResult();
			return preco.getPreco();
		} catch (Exception e) {
			return null;
		}
	}

	public void alteraDescricao(String codigo, String descricao) {
		Produto produto = manager.find(Produto.class, codigo);
		produto.setDescricao(descricao);
	}

	public void atualizaPrecos(String mercado, String precos) {
		Scanner scanner = new Scanner(precos);
		while (scanner.hasNextLine()) {
			try {
				String str = scanner.nextLine();

				String codigo = str.split(";")[0];
				BigDecimal valor = new BigDecimal(str.split(";")[1].replace(",", "."));

				String JPQL = createJPQL(codigo, mercado);
				Query query = manager.createQuery(JPQL);
				query.setParameter("pPreco", valor);
				query.setParameter("pCodigo", codigo);
				query.setParameter("pMercado", mercado);
				query.executeUpdate();
			} catch (Exception e) {
				continue;
			}
		}
		scanner.close();
	}

	private String createJPQL(String codigo, String mercado) {
		return precoExiste(codigo, mercado)
				? "update PrecosVenda p set p.preco = :pPreco where p.codigo = :pCodigo and p.mercado = :pMercado"
				: "insert into PrecosVenda p (codigo, preco, mercado) values (:pCodigo, :pPreco, :pMercado)";
	}

	private boolean precoExiste(String codigo, String mercado) {
		String JPQL = "select p.codigo from PrecosVenda p where p.codigo = :pCodigo and p.mercado = :pMercado";
		Query findQuery = manager.createQuery(JPQL);
		findQuery.setParameter("pCodigo", codigo);
		findQuery.setParameter("pMercado", mercado);
		String singleResult = (String) findQuery.getSingleResult();
		return singleResult != null ? true : false;
	}


	public void alteraMarkup(String codigo, float markup) {
		Produto produto = manager.find(Produto.class, codigo);
		produto.setMarkup(markup);
	}

	public void removeMateriaPrima(String codigo) {
		manager.find(Produto.class, codigo).setMateriaPrima(null).setQtdMateriaPrima(0);
	}

	public void removeProcesso(long id) {
		manager.remove(id);
	}

	public void addProcesso(Processo processo) {
		manager.find(Produto.class, processo.getProduto().getCodigo()).addProcesso(processo);
	}
}
