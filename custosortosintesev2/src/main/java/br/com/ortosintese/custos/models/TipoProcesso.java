package br.com.ortosintese.custos.models;

public enum TipoProcesso {
	INTERNO, EXTERNO;
}
