package br.com.ortosintese.custos.controllers;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.ortosintese.custos.models.Processo;
import br.com.ortosintese.custos.models.Produto;

@Repository
@Transactional
public class ProcessoDAO {

	@PersistenceContext
	EntityManager manager;

	public Processo find(Processo tempProcesso, Produto produto) {
		Query query = manager.createQuery(
				"select p from Produto_processos p where produto=:pProduto and fornecedor=:pFornecedor and descricao=:pDescricao");
		query.setParameter("pProduto", produto.getCodigo());
		query.setParameter("pFornecedor", tempProcesso.getFornecedor());
		query.setParameter("pDescricao", tempProcesso.getDescricao());
		Processo processo = (Processo) query.getSingleResult();
		return processo;
	}

	public Processo find(long id) {
		return manager.find(Processo.class, id);
	}

	public void salva(Processo processo) {
		if (manager.find(Processo.class, processo.getId()) == null) {
			manager.persist(processo);
		} else {
			manager.merge(processo);
		}
	}

	public void remove(long id) {
		Processo processo = manager.find(Processo.class, id);
		manager.remove(processo);
	}
}
