package br.com.ortosintese.custos.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.ortosintese.custos.models.MateriaPrima;
import br.com.ortosintese.custos.models.UnidadeMedida;

@Repository
@Transactional
public class MateriaPrimaDAO {

	@PersistenceContext
	EntityManager manager;

	public void persist(MateriaPrima materiaPrima) {
		manager.persist(materiaPrima);
	}

	public MateriaPrima find(int id) {
		return manager.find(MateriaPrima.class, id);
	}

	public Iterable<MateriaPrima> findall() {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<MateriaPrima> query = criteriaBuilder.createQuery(MateriaPrima.class);
		Root<MateriaPrima> root = query.from(MateriaPrima.class);
		query.select(root);
		query.orderBy(criteriaBuilder.asc(root.<String> get("descricao")));
		return manager.createQuery(query).getResultList();
	}

	public List<MateriaPrima> findByDescricaoAndUnidadeMedidaAndValor(String descricao, UnidadeMedida unidadeMedida, BigDecimal valorMenor,
			BigDecimal valorMaior) {
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		CriteriaQuery<MateriaPrima> query = criteriaBuilder.createQuery(MateriaPrima.class);
		Root<MateriaPrima> root = query.from(MateriaPrima.class);
		
		ArrayList<Predicate> predicates = new ArrayList<>();

		if (descricao != null) {
			Path<String> descricaoPath = root.<String> get("descricao");
			Predicate predicateDescricaoLike = criteriaBuilder.like(descricaoPath, "%" + descricao + "%");
			predicates.add(predicateDescricaoLike);
		}
		
		if (unidadeMedida != null) {
			Path<UnidadeMedida> unidadeMedidaPath = root.<UnidadeMedida>get("unidadeMedida");
			Predicate predicateUnidadeMedidaEqual = criteriaBuilder.equal(unidadeMedidaPath, unidadeMedida);
			predicates.add(predicateUnidadeMedidaEqual);
		}
		
		if (valorMenor != null && valorMaior != null) {
			Path<BigDecimal> custoPorUMPath = root.<BigDecimal>get("custoPorUM");
			Predicate predicateCustoPorUMEqual = criteriaBuilder.between(custoPorUMPath, valorMenor, valorMaior);
			predicates.add(predicateCustoPorUMEqual);
		}
		
		query.where(predicates.toArray(new Predicate[0]));
		query.select(root);
		query.orderBy(criteriaBuilder.asc(root.get("descricao")));
		
		return manager.createQuery(query).getResultList();

	}

	public void remove(int id) {
		MateriaPrima materiaPrima = manager.find(MateriaPrima.class, id);
		manager.remove(materiaPrima);
	}

	public void altera(MateriaPrima materiaPrima) {
		if (manager.find(MateriaPrima.class, materiaPrima.getId()) != null) {
			manager.merge(materiaPrima);
		}
	}
}
