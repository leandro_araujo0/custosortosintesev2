package br.com.ortosintese.custos.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MateriaPrima {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String descricao;
	@Enumerated(EnumType.STRING)
	private UnidadeMedida unidadeMedida;
	private BigDecimal custoPorUM;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public UnidadeMedida getUnidadeMedida() {
		return unidadeMedida;
	}

	public void setUnidadeMedida(UnidadeMedida unidadeMedida) {
		this.unidadeMedida = unidadeMedida;
	}

	public BigDecimal getCustoPorUM() {
		return custoPorUM;
	}

	public void setCustoPorUM(BigDecimal custoPorUM) {
		this.custoPorUM = custoPorUM;
	}

	@Override
	public String toString() {
		return "MateriaPrima [id=" + id + ", descricao=" + descricao + ", unidadeMedida=" + unidadeMedida
				+ ", custoPorUM=" + custoPorUM + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
