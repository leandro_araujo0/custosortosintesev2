package br.com.ortosintese.custos.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Componente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@OneToOne
	private Produto produto;
	@OneToOne
	private Produto produtoFilho;
	private BigDecimal quantidade;

	public Produto getProdutoFilho() {
		return produtoFilho;
	}

	public Componente setProdutoFilho(Produto produtoFilho) {
		this.produtoFilho = produtoFilho;
		return this;
	}

	public BigDecimal getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(BigDecimal quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getCustoTotal() {
		return produtoFilho.getCusto().multiply(quantidade).setScale(2);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((produtoFilho == null) ? 0 : produtoFilho.hashCode());
		result = prime * result + ((quantidade == null) ? 0 : quantidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Componente other = (Componente) obj;
		if (produtoFilho == null) {
			if (other.produtoFilho != null)
				return false;
		} else if (!produtoFilho.equals(other.produtoFilho))
			return false;
		if (quantidade == null) {
			if (other.quantidade != null)
				return false;
		} else if (!quantidade.equals(other.quantidade))
			return false;
		return true;
	}
}