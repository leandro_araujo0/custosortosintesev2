package br.com.ortosintese.custos.models;

public enum UnidadeMedida {
	KG, M, PC;
}
