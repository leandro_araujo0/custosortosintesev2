package br.com.ortosintese.custos.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Processo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@OneToOne
	private Produto produto;
	private String fornecedor;
	private String descricao;
	private double tempo;
	private BigDecimal custoPorHora;
	private BigDecimal valor;
	private long lote = 1;
	private String observacao;
	private TipoProcesso tipoProcesso = TipoProcesso.INTERNO;

	public Processo(String fornecedor, String descricao) {
		this.fornecedor = fornecedor;
		this.descricao = descricao;
	}

	public Processo() {
	}

	public String getDetalhes() {
		String detalhes = new String();
		if (tipoProcesso.equals(TipoProcesso.INTERNO)) {
			detalhes += "Tempo: " + Ferramentas.tempoDoubleToHora(tempo) + " Custo por Hora: " + custoPorHora + " - "
					+ observacao;
		} else {
			detalhes += observacao;
		}
		return detalhes;
	}

	@Override
	public String toString() {
		return "Processo [fornecedor=" + fornecedor + ", descricao=" + descricao + ", tempo=" + tempo
				+ ", custoPorHora=" + custoPorHora + ", valor=" + valor + ", lote=" + lote + ", observacao="
				+ observacao + "]";
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public Processo setFornecedor(String fornecedor) {
		this.fornecedor = fornecedor;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public Processo setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public double getTempo() {
		return tempo;
	}

	public void setTempo(double tempo) {
		this.tempo = tempo;
	}

	public BigDecimal getCustoPorHora() {
		return custoPorHora;
	}

	public void setCustoPorHora(BigDecimal bigDecimal) {
		this.custoPorHora = bigDecimal;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public long getLote() {
		return lote;
	}

	public void setLote(long lote) {
		this.lote = lote;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public TipoProcesso getTipoProcesso() {
		return tipoProcesso;
	}

	public void setTipoProcesso(TipoProcesso tipoProcesso) {
		this.tipoProcesso = tipoProcesso;
	}

	public BigDecimal getCusto() {
		if (tipoProcesso.equals(TipoProcesso.INTERNO)) {
			return custoPorHora.multiply(new BigDecimal(tempo)).divide(new BigDecimal(lote), 2).setScale(2,
					RoundingMode.HALF_EVEN);
		} else {
			return valor.divide(new BigDecimal(lote), 2).setScale(2, RoundingMode.HALF_EVEN);
		}
	}

	public BigDecimal getCustoTotal() {
		return getCusto().multiply(new BigDecimal(lote)).setScale(2, RoundingMode.HALF_EVEN);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((custoPorHora == null) ? 0 : custoPorHora.hashCode());
		result = prime * result + ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result + ((fornecedor == null) ? 0 : fornecedor.hashCode());
		result = prime * result + (int) (lote ^ (lote >>> 32));
		result = prime * result + ((observacao == null) ? 0 : observacao.hashCode());
		long temp;
		temp = Double.doubleToLongBits(tempo);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((tipoProcesso == null) ? 0 : tipoProcesso.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Processo other = (Processo) obj;
		if (custoPorHora == null) {
			if (other.custoPorHora != null)
				return false;
		} else if (!custoPorHora.equals(other.custoPorHora))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (fornecedor == null) {
			if (other.fornecedor != null)
				return false;
		} else if (!fornecedor.equals(other.fornecedor))
			return false;
		if (lote != other.lote)
			return false;
		if (observacao == null) {
			if (other.observacao != null)
				return false;
		} else if (!observacao.equals(other.observacao))
			return false;
		if (Double.doubleToLongBits(tempo) != Double.doubleToLongBits(other.tempo))
			return false;
		if (tipoProcesso != other.tipoProcesso)
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}
}
