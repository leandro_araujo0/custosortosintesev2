function escondeProcessoInterno() {
	$('.processo-externo').removeClass('hidden');
	$('.processo-externo > input').empty();
	$('.processo-interno').addClass('hidden');
}

function escondeProcessoExterno() {
	$('.processo-interno').removeClass('hidden');
	$('.processo-interno > input').empty();
	$('.processo-externo').addClass('hidden');
}

function autoCompletaTempo() {
	var input = document.querySelector('#tempo');
	input.addEventListener('input', function() {
		if (input.value.length == 2 || input.value.length == 5) {
			input.value += ':'
		}
	});
}

function calculaTempo() {
	var x = $('#tempo')[0];
	if (x.value.includes(':')) {
		var strs = x.value.split(':');
		var tempoHoras = parseFloat(strs[0]);
		var tempoMinutos = parseFloat(strs[1]);
		var tempoSegundos = parseFloat(strs[2]);
		x.value = Math.round(parseFloat(tempoHoras + tempoMinutos / 60 + tempoSegundos / 3600) * 100000) / 100000;
	}
}