<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<section id="main" class="container">
			<h1>Ortosintese</h1>
		</section>
	</body>
</html>