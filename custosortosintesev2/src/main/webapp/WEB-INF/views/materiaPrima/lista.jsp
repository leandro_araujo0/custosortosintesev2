<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>	
		<section id="main" class="container-fluid">
			<h2>Lista Materias Primas</h2>
			<div class="row">
				<div class="col-md-2">
					<form id="form-procurar" action="${s:mvcUrl('MPC#lista').build()}" method="get">
						<div class="form-group">
							<label>Descricao</label>
							<input class="form-control" type="text" name="descricao" autofocus>
						</div>
						<div class="form-group">
							<label>UnidadeMedida</label>
							<select class="form-control" name="unidadeMedida">
								<option value=""></option>
								<c:forEach var="unidadeMedida" items="${unidadesMedidas}">
									<option value="${unidadeMedida}">${unidadeMedida}</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group">
							<label>Valor</label>
							<input class="form-control" type="text" name="valorMenor" placeholder="Valor Menor">
							<input class="form-control" type="text" name="valorMaior" placeholder="Valor Maior">
						</div>
						<button class="btn btn-default" type="submit">Filtrar</button>
					</form>
				</div>
				<div class="col-md-10">
					<a class="btn btn-default" href="${s:mvcUrl('MPC#form').build()}">Adicionar</a>
					<table class="table">
						<thead>
							<tr>
								<th>Descricao</th>
								<th>Unidade</th>
								<th>Custo</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="materiaPrima" items="${materiasPrimas}">
								<tr>
									<td><a href="${s:mvcUrl('MPC#detalhes').arg(0, materiaPrima.id).build()}">${materiaPrima.descricao}</a></td>
									<td>${materiaPrima.unidadeMedida}</td>
									<td>${materiaPrima.custoPorUM}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</body>
</html>