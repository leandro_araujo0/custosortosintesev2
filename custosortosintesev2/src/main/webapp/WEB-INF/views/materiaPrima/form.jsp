<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<form:form class="form" action="${s:mvcUrl('MPC#cadastrar').build()}" method="post" commandName="materiaPrima">
				<div class="form-group">
					<label>Descricao</label>
					<form:input class="form-control" path="descricao" autofocus="autofocus"/>
				</div>
				<div class="form-group">
					<label>UM</label>
					<form:select class="form-control" path="unidadeMedida">
						<form:options items="${unidadesMedidas}"/>
					</form:select>
				</div>
				<div class="form-group">
					<label>Custo Por UM</label>
					<form:input class="form-control" path="custoPorUM"/>
				</div>
				<button class="btn btn-default" type="submit">Cadastrar</button>
			</form:form>
		</section>
	</body>
</html>