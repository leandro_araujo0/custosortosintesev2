<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<div class="container">
			<table class="table">
				<thead>
					<tr>
						<th>Produto</th>
						<th>Fornecedor</th>
						<th>Processo</th>
						<th>Valor</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="produto" items=${produtos}>
							<c:forEach var="processo" items=${produto.processos}>
								<tr>
										<td>${produto.codigo}</td>
										<td>${processo.fornecedor}</td>
										<td>${processo.descricao}</td>
										<td>${processo.custoUnitario}</td>
								</tr>
							</c:forEach>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</body>
</html>