<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ortosintese</title>
<c:url value="/resources/" var="pathRes"/>
<link rel="stylesheet" href="${pathRes}/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="${pathRes}/css/site.css">
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>