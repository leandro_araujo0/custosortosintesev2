<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<nav id="acoes" class="navbar navbar-default">
	<div class="navbar-header">
		<a class="navbar-brand" href="${s:mvcUrl('PC#form').build()}">Cadastrar</a>
		<a class="navbar-brand" href="${s:mvcUrl('PC#lista').build()}/">Lista</a>
		<a class="navbar-brand" href="${s:mvcUrl('MPC#lista').build()}">Materias Primas</a>
		<a class="navbar-brand" href="${s:mvcUrl('PC#atualizarPrecosForm').build()}">Atualizar Precos</a>
	</div>
</nav>
<c:url value="/resources/js/" var="jsPath" />
<script src="${jsPath}custos.js"></script>