<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>"/>
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<h2>Últimas Atualizações</h2>
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>Codigo</th>
							<th>Descricao</th>
							<th>Custo Total</th>
							<th>Data</th>
							<th>Preco MI</th>
							<th>Preco ME</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="produto" items="${produtos}">
							<tr>
								<td><a href="${s:mvcUrl('PC#detalhe').arg(0, produto.codigo).build()}">${produto.codigo}</a></td>
								<td>${produto.descricao}</td>
								<td>R$ ${produto.custo}</td>
								<td>Ultima revisão: ${dateFormat.format(produto.revisao.time)}</td>
								<c:if test="${not empty produto.getPreco(dao, 'MI') and not produto.custoZero}">
									<td>Preco MI: R$ ${produto.getPreco(dao, 'MI')} (Mkup: ${produto.getPreco(dao, 'MI') / produto.custo})</td>
								</c:if>
								<c:if test="${not empty produto.getPreco(dao, 'ME') and not produto.custoZero}">
									<td>Preco ME: U$ ${produto.getPreco(dao, 'ME')} (Mkup: ${produto.getPreco(dao, 'ME') * 3 / produto.custo})</td>
								</c:if>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</section>
	</body>
</html>