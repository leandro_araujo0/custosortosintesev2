<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container-fluid">
			<h2>Produtos Críticos</h2>
			<div class="row">
				<div class="col-md-2">
					<form:form id="form-procurar" class="form" action="${s:mvcUrl('PC#lista').build()}" method="get">
						<div class="form-group">
							<label>Codigo</label>
							<input class="form-control" type="text" name="codigo" autofocus>
						</div>
						<div class="form-group">
							<label>Descricao</label>
							<input class="form-control" type="text" name="descricao">
						</div>
						<button class="btn btn-default" type="submit">Filtrar</button>
					</form:form>
				</div>
				<div class="table-responsive col-md-10">
					<table class="table">
						<thead>
							<tr>
								<th>Produto</th>
								<th>Descricao</th>
								<th>Custo Total</th>
								<th>Ultima Revisao</th>
								<th>Preco MI</th>
								<th>Preco ME</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="produto" items="${produtos}">
								<tr>
									<td><a href="${s:mvcUrl('PC#detalhe').build()}=${produto.codigo}">${produto.codigo}</a></td>
									<td>${produto.descricao}</td>
									<td>${produto.custo}</td>
									<td>${dateFormat.format(produto.revisao.time)}</td>
									<c:if test="${not empty produto.getPreco(dao, 'MI') and not produto.custoZero}">
										<td>Preco MI: R$ ${produto.getPreco(dao, 'MI')} (Mkup: ${produto.getPreco(dao, 'MI') / produto.custo})</td>
									</c:if>
									<c:if test="${not empty produto.getPreco(dao, 'ME') and not produto.custoZero}">
										<td>Preco ME: U$ ${produto.getPreco(dao, 'ME')} (Mkup: ${produto.getPreco(dao, 'ME') * 3 / produto.custo})</td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</section>
	</body>
</html>