<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href='<c:url value="/resources/css/acoes.css"/>'>
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<h2>Cadastro de Produto</h2>
			<form:form class="form" action="${s:mvcUrl('PC#cadastro').build()}" method="POST" commandName="produto" id="form">
				<div class="form-group">
					<label>Data</label>					
					<p class="form-control-static">${data}</p>
				</div>
				<div class="row">
					<div class="form-group col-md-3">
						<label>Codigo</label>
						<form:input class="form-control" path="codigo" required="required" autofocus="autofocus"/>
					</div>
					<div class="form-group col-md-9">
						<label>Descricao</label>
						<form:input class="form-control" path="descricao" required="required"/>
					</div>
				</div>
				<button type="submit" class="btn btn-default">Cadastrar</button>
			</form:form>
		</section>
	</body>
</html>