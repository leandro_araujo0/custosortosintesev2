<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value='/resources/css/componenteForm.css'/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<section id="main" class="container">
			<h2><a href="${s:mvcUrl('PC#detalhe').arg(0, produto.codigo).build()}">${produto.codigo} ${produto.descricao}</a></h2>
			<form:form class="form-inline" action="${s:mvcUrl(actionName).build()}" method="POST" commandName="componente">
				<input type="hidden" name="codigo" value="${produto.codigo}">
				<div class="form-group">
					<label>Codigo</label>
					<div class="input-group">
						<form:input class="form-control" path="produtoFilho.codigo" required="required" autofocus="autofocus"/>
						<div class="input-group-addon"><a class="glyphicon glyphicon-search" onclick="$('.form-procurar').toggleClass('hidden')"></a></div>
					</div>
				</div>
				<div class="form-group">
					<label>Quantidade</label>
					<form:input class="form-control" path="quantidade" required="required"/>
				</div>
				<button class="btn btn-default" type="submit">${btnName}</button>
			</form:form>
			<form class="form-inline form-procurar hidden">
				<label>Procurar</label>
				<input id="procurar" class="form-control" type="text">
				<a class="btn btn-default" href="javascript:pesquisaResultados()">Procurar</a>
			</form>
			<div id="resultados" class="hidden">
				<h3>Resultados</h3>
			</div>
		</section>
		<script src="<c:url value='/resources/js/custos.js'/>"></script>
	</body>
</html>