<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>">
		<link rel="stylesheet" href="<c:url value="/resources/css/detalhe.css"/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<h1>${produto.codigo} ${produto.descricao}</h1>
			<h2>Matéria Prima</h2>
			<p>${produto.materiaPrima.descricao}</p>
			<p>Unidade de Medida: ${produto.materiaPrima.unidadeMedida} - Valor por UM: R$ ${produto.materiaPrima.custoPorUM}</p>
			<p>Quantidade: ${produto.qtdMateriaPrima} - Total: R$ ${produto.custoMateriaPrima}</p>
			<c:if test="${produto.processos.size() > 0}">
				<h2>Processos</h2>
			</c:if>
			<c:forEach var="processo" items="${produto.processos}">
				<p>${processo.fornecedor} - ${processo.descricao} <c:if test="${not empty processo.observacao}">(${processo.observacao})</c:if> - Custo Total: ${processo.custoTotal} - Peças: ${processo.lote} - Custo Unitário: ${processo.custo}</p>
			</c:forEach>
			<c:if test="${produto.componentes.size() > 0}">
				<h2>Componentes</h2>
			</c:if>
			<c:forEach var="componente" items="${produto.componentes}">
				<p>${componente.produtoFilho.codigo} - ${componente.produtoFilho.descricao} - Custo: ${componente.produtoFilho.custo} - Quantidade: ${componente.quantidade} - Custo Total: ${componente.custoTotal}</p>
			</c:forEach>
			<h2>Custo Total: R$ ${produto.custo}</h2>
		</section>
	</body>
</html>