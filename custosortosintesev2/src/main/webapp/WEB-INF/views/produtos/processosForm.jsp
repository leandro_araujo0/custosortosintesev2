<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href='<c:url value="/resources/css/acoes.css"/>'>
		<script type="text/javascript" src="<c:url value="/resources/js/processoForm.js"/>"></script>
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<h2><a href="${s:mvcUrl('PC#detalhe').arg(0, produto.codigo).build()}">${produto.codigo} ${produto.descricao}</a></h2>
			<form:form class="form" action="${s:mvcUrl(actionName).build()}" method="POST" commandName="processo">
				<form:hidden path="id"/>
				<form:hidden path="produto.codigo"/>
				<input type="hidden" name="codigo" value="${produto.codigo}">
				<input type="hidden" name="descricaoProcessoAntigo" value="${processo.descricao}">
				<input type="hidden" name="fornecedorProcessoAntigo" value="${processo.fornecedor}">
				
				<div class="form-group">
					<label><form:radiobutton path="tipoProcesso" value="INTERNO" onclick="javascript:escondeProcessoExterno()"  autofocus="autofocus"/>Interno</label>
					<label><form:radiobutton path="tipoProcesso" value="EXTERNO" onclick="javascript:escondeProcessoInterno()"/>Externo</label>
				</div>
				<div class="form-group processo-externo <c:if test="${processo.tipoProcesso eq 'INTERNO'}">hidden</c:if>">
					<label>Fornecedor</label>
					<form:input class="form-control" path="fornecedor"/>
				</div>
				<div class="form-group processo-externo <c:if test="${processo.tipoProcesso eq 'INTERNO'}">hidden</c:if>">
					<label>Descricao</label>
					<form:input class="form-control" path="descricao"/>
				</div>
				<div class="form-group processo-interno <c:if test="${processo.tipoProcesso eq 'EXTERNO'}">hidden</c:if>">
					<label>Recurso</label>
					<form:select class="form-control" path="descricao">
						<c:forEach var="recurso" items="${recursos}">
							<form:option value="${recurso.descricao}">${recurso.descricao}</form:option>
						</c:forEach>
					</form:select>
				</div>
				<div class="form-group processo-interno <c:if test="${processo.tipoProcesso eq 'EXTERNO'}">hidden</c:if>">
					<label>Tempo</label>
					<div class="input-group">
						<div class="input-group-addon" onclick="javascript:calculaTempo()">hh:mm:ss</div>
						<form:input id="tempo" class="form-control" path="tempo"/>
					</div>
				</div>
				<div class="form-group processo-externo <c:if test="${processo.tipoProcesso eq 'INTERNO'}">hidden</c:if>">
					<label>Valor</label>
					<form:input class="form-control" path="valor"/>
				</div>
				<div class="form-group">
					<label>Lote</label>
					<form:input class="form-control" path="lote"/>
				</div>
				<div class="form-group">
					<label>Observacao</label>
					<form:input class="form-control" path="observacao"/>
				</div>
				<button class="btn btn-default" type="submit">${btnName}</button>
			</form:form>
		</section>
	</body>
</html>