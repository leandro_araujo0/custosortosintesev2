<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
    
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
	<link rel="stylesheet" href="<c:url value="/resources/css/acoes.css"/>">
	<link rel="stylesheet" href="<c:url value="/resources/css/detalhe.css"/>">
</head>
<body>
	<c:import url="/WEB-INF/views/_common/nav.jsp"/>
	<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
	<section id="main" class="container">
		<form class="form" method="post">
			<div class="form-group">
				<label>Mercado</label>
				<select class="form-control" name="mercado">
					<option value="MI">Interno</option>
					<option value="ME">Externo</option>
				</select>
			</div>
			<div class="form-group">
				<label>Precos</label>
				<textarea class="form-control" rows="20" cols="100" name="precos"></textarea>
			</div>
			<div class="form-group">
				<button class="btn btn-default" type="submit">Enviar</button>
			</div>
		</form>
	</section>
</body>
</html>