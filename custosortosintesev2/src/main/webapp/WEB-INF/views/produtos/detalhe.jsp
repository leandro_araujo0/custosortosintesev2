<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<c:import url="/WEB-INF/views/_common/bootstrap.jsp"/>
		<link rel="stylesheet" href="<c:url value="/resources/css/detalhe.css"/>">
	</head>
	<body>
		<c:import url="/WEB-INF/views/_common/nav.jsp"/>
		<c:import url="/WEB-INF/views/_common/acoesProdutos.jsp"/>
		<section id="main" class="container">
			<div class="row">
				<div class="row col-md-8">
					<h2 class="col-md-12">${produto.codigo} - ${produto.descricao}</h2>
					<a id="botao-impressao" class="btn btn-default" href="${s:mvcUrl('PC#impressao').arg(0, produto.codigo).build()}">Impressão</a>
					<a id="botao-danger" class="btn btn-danger" href="${s:mvcUrl('PC#excluirProduto').arg(0, produto.codigo).build()}">Excluir Produto!</a>
				</div>
				<div class="row col-md-4">
					<p class="custo-total col-md-12">Custo Total: R$ ${produto.custo}</p>
					<c:if test="${not empty produto.getPreco(dao, 'MI') and not produto.custoZero}">
						<p class="col-md-12">Preco MI: R$ ${produto.getPreco(dao, 'MI')} (Mkup: ${produto.getPreco(dao, 'MI') / produto.custo})</p>
					</c:if>
					<c:if test="${not empty produto.getPreco(dao, 'ME') and not produto.custoZero}">
						<p class="col-md-12">Preco ME: U$ ${produto.getPreco(dao, 'ME')} (Mkup: ${produto.getPreco(dao, 'ME') * 3 / produto.custo})</p>
					</c:if>
					<p class="col-md-12">Ultima revisão: ${dateFormat.format(produto.revisao.time)}</p>
				</div>
			</div>
			<form:form class="form" action="${s:mvcUrl('PC#alteraDescricao').build()}" method="post" commandName="produto">
				<form:input type="hidden" value="${produto.codigo}" path="codigo"/>
				<div class="row">
					<div class="form-group col-md-10">
						<label>Descricao</label>
						<form:input class="form-control" path="descricao"/>
					</div>
					<div class="form-group col-md-2">
						<label>MarkUp Mínimo</label>
						<form:input class="form-control" path="markup"/>
					</div>
				</div>
				<button class="btn btn-default" type="submit">Alterar</button>
			</form:form>
			<form:form id="form-materia-prima" class="form" action="${s:mvcUrl('PC#alteraMateriaPrima').build()}" method="post" commandName="produto">
				<form:input type="hidden" path="codigo"/>
				<div class="row">
					<div class="form-group col-xs-12 col-md-8">
						<label>Materia Prima</label>
						<form:select class="form-control" path="materiaPrima.id">
						<option value=""></option>
							<c:forEach var="materiaPrima" items="${materiasPrimas}">
								<option value="${materiaPrima.id}" <c:if test="${materiaPrima.id eq produto.materiaPrima.id}">selected</c:if>>${materiaPrima.descricao} - Un: ${materiaPrima.unidadeMedida} - Custo: R$ ${materiaPrima.custoPorUM}</option>
							</c:forEach>
						</form:select>
					</div>
					<div class="form-group col-xs-6 col-md-2">
						<label>Qtd MP</label>
						<form:input class="form-control" path="qtdMateriaPrima"/>
					</div>
					<div class="form-group col-xs-6 col-md-2">
						<label>Valor MP</label>
						<input class="form-control" value="R$ ${produto.custoMateriaPrima}" disabled></input>
					</div>
				</div>
				<button class="btn btn-default" type="submit">Alterar</button>
				<a class="btn btn-default" href="${s:mvcUrl('PC#removeMateriaPrima').arg(0, produto.codigo).build()}">Remover</a>
			</form:form>
			<h2>Processos</h2>
			<a href="${s:mvcUrl('PC#processoForm').arg(0, produto.codigo).build()}">Adicionar Processo</a>
			<table class="table">
				<thead>
					<tr>
						<th>Fornecedor</th>
						<th>Descricao</th>
						<th>Valor</th>
						<th>Lote</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="processo" items="${produto.processos}" varStatus="loop">
						<tr title="${processo.detalhes}">
							<td>${processo.fornecedor}</td>
							<td>${processo.descricao}</td>
							<td>${processo.custoTotal}</td>
							<td>${processo.lote}</td>
							<td>${processo.custo}</td>
							<td>
								<div class="dropdown">
						          	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu${loop.index}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						            	Acoes
						            	<span class="caret"></span>
						          	</button>
						          	<ul class="dropdown-menu" aria-labelledby="dropdownMenu${loop.index}">
							            <li><a href="${s:mvcUrl('PC#alteraProcessoForm').arg(0, processo.id).build()}">Alterar</a></li>
							            <li><a href="${s:mvcUrl('PC#removeProcesso').arg(0, processo.id).build()}">Remover</a></li>
						          	</ul>
						        </div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<h2>Componentes</h2>
			<a href="${s:mvcUrl('PC#cadastraComponenteForm').arg(0, produto.codigo).build()}">Adicionar Componente</a>
			<table class="table">
				<thead>
					<tr>
						<th>Código</th>
						<th>Descricao</th>
						<th>Custo</th>
						<th>Quantidade</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="componente" items="${produto.componentes}">
						<tr>
							<td><a href="${s:mvcUrl('PC#detalhe').arg(0, componente.produtoFilho.codigo).build()}">${componente.produtoFilho.codigo}</a></td>
							<td>${componente.produtoFilho.descricao}</td>
							<td>${componente.produtoFilho.custo}</td>
							<td>${componente.quantidade}</td>
							<td>${componente.custoTotal}</td>
							<td>
								<div class="dropdown">
						          	<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu${loop.index}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						            	Acoes
						            	<span class="caret"></span>
						          	</button>
						          	<ul class="dropdown-menu" aria-labelledby="dropdownMenu${loop.index}">
							            <li><a href="${s:mvcUrl('PC#alteraComponenteForm').arg(0, produto.codigo).arg(1, componente.produtoFilho.codigo).arg(2, componente.quantidade).build()}">Alterar</a></li>
							            <li><a href="${s:mvcUrl('PC#removeComponente').arg(0, produto.codigo).arg(1, componente.produtoFilho.codigo).build()}">Remover</a></li>
						          	</ul>
						        </div>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</section>
	</body>
</html>